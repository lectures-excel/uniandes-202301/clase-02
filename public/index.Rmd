---
pagetitle: "Clase 2: Leer/Escribir conjuntos de datos"
title: "Clase 2: Leer/Escribir conjuntos de datos"
subtitle: Taller de Excel | [ECON-1300](https://bloqueneon.uniandes.edu.co/d2l/home/208697)
author: 
      name: Eduard-Martínez
      affiliation: Universidad de los Andes  #[`r fontawesome::fa('globe')`]()
# date: Lecture 10  #"`r format(Sys.time(), '%d %B %Y')`"
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--==================-->
<!--==================-->
## **[1.] Leer y escribir archivos desde diferentes formatos**

### **1.1. Leer archivos .accdb (ACCES)**

**Paso 1: Seleccionar archivo:**

![](pics/accdb_1.png){width=40%}

**Paso 2: Seleccionar tabla a importar:**

![](pics/accdb_2.png){width=40%}

**Paso 3: Definir ubicación en la hoja:**

![](pics/accdb_3.png){width=40%}

**Paso 4: Inspeccionar conjunto de datos:**

![](pics/accdb_4.png){width=40%}

<!--------------------->
### **1.2. Leer archivos .csv, .tsv o .txt**

**Paso 1: Seleccionar archivo:**

![](pics/csv_1.png){width=40%}

**Paso 2: Ancho fijo o delimitador?:**

![](pics/csv_2.png){width=40%}

**Paso 3: Definir delimitador:**

![](pics/csv_3.png){width=40%}

**Paso 4: Definir ubicación en la hoja:**

![](pics/csv_4.png){width=40%}

**Paso 5: Inspeccionar conjunto de datos:**

![](pics/csv_5.png){width=40%}

<!--==================-->
<!--==================-->
## **[2.] Fórmula y operadores**

Los operadores son símbolos o caracteres que especifican algún tipo de operación a realizar. En una misma fórmula podrían participar diferentes operadores. Puede acceder a la lista completa de funciones de Excel aquí [https://support.microsoft.com](https://support.microsoft.com/es-es/office/funciones-de-excel-por-orden-alfabético-b3944572-255d-4efb-bb96-c6d90033e188) y en inglés [aquí](https://support.microsoft.com/en-us/office/excel-functions-alphabetical-b3944572-255d-4efb-bb96-c6d90033e188)

<!------------->
### **2.1. Operadores aritméticos**

También conocidos como operadores matemáticos:

![](pics/operador_1.png){width=40%}

<!------------->
### **2.2 Operadores de comparación**

También llamados operadores lógicos o relacionales, nos permite realizar comparaciones entre dos o más números o cadenas de texto:

![](pics/operador_2.png){width=40%}

<!------------->
### **2.3 Operadores de concatenación**

También llamados operadores de texto:

![](pics/operador_3.png){width=40%}

<!------------->
### **2.4 Operadores de referencia**

![](pics/operador_4.png){width=40%}

<!--==================-->
<!--==================-->
## **[3.] Referencias relativas y absolutas**

Una referencia es el nombre/dirección que permite identificar una celda o un rango de celdas dentro de un libro de Excel. 
![](pics/ref_1.png){width=40%}

<!--------------------->
### **3.1 Referencias relativas**

Las referencias relativas son aquellas que llaman una operación dentro de una celda en Excel. Esta referencia no es fija, es decir al momento de arrastrar la formula en alguna dirección, la referencia también se moverá:

![](pics/ref_2.png){width=80%}

<!--------------------->
### **3.2 Referencias absolutas**

Las referencias absolutas son aquellas fijan una operación desde una celda de Excel. Se debe agregar el signo **$** antes y después de la letra de columna. Lo anterior fija la operación a esa celda haciendo que al momento de arrastrar la formula a otras celdas la referencia se mantenga estable.

![](pics/ref_3.png){width=80%}

La siguiente tabla resume cómo se actualizará un tipo de referencia si una fórmula que contiene la referencia se copia dos celdas hacia abajo y dos celdas a la derecha:

![](pics/ref_4.png){width=40%}

<!----------------->
<!--- Checklist --->
<!----------------->
## **[4.] Task**

#### **1.** Importe el archivo "task_clase-02.xlsx" disponible [aquí](https://gitlab.com/lectures-excel/taller-excel/clase-02/-/archive/main/clase-02-main.zip?path=task).
#### **2.** Responda las preguntas de **Tabla 1** en la hoja **task**.
#### **3.** Guarde el libro en formato .xlsx, asígnele su código como nombre del archivo y súbalo a Bloque Neón. Por ejemplo: **201725842.xlsx**
